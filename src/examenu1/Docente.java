/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenu1;

/**
 *
 * @author Enrique
 */
public class Docente extends Empleado{

    private float horas, pagoHora;
    private int nivel;

    public Docente(float horas, float pagoHora, int nivel) {
        this.horas = horas;
        this.pagoHora = pagoHora;
        this.nivel = nivel;
    }
    public Docente() {
        this.nivel = 0;
        this.horas = 0.0f;
        this.pagoHora = 0.0f;
    }


    public float getHoras() {
        return horas;
    }

    public void setHoras(float horas) {
        this.horas = horas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    

    @Override
      public float calcularTotal() {
        float total=0.0f;
        if(this.nivel==0) total=(this.pagoHora * this.horas*1.35f);
        if(this.nivel==1) total=(this.pagoHora * this.horas*1.4f);
        if(this.nivel==2) total=(this.pagoHora * this.horas*1.5f);
        return total;
    }

    @Override
    public float calcularImpuesto() {
        return this.horas * this.pagoHora * this.contrato.getImpuesto()/100;
    }
    
}
