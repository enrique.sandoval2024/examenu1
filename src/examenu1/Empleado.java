/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenu1;

/**
 *
 * @author Enrique
 */
public abstract class Empleado {
    
     protected int  numEmp;
    protected String nombre, domicilio;
    protected Contrato contrato;

 
    
     public Empleado() {
        this.numEmp = 0;
        this.nombre = "";
        this.domicilio = "";
        this.contrato = new Contrato();
    }

    public int getNumEmp() {
        return numEmp;
    }

    public void setNumEmp(int numEmp) {
        this.numEmp = numEmp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }
    
    public abstract float calcularImpuesto();
        
    
    
    public abstract float calcularTotal();
        
    
    
    
}
