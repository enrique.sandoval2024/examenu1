/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenu1;

import javax.swing.JOptionPane;

/**
 *
 * @author Enrique
 */
public class jintEmpleado extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintEmpleado
     */
    public jintEmpleado() {
        initComponents();
        this.resize(600, 600);
        this.deshabilitar();
    }
    public void habilitar(){
        this.txtClave.setEnabled(true);
        this.txtDom.setEnabled(true);
        this.txtHoras.setEnabled(true);
        this.txtPuesto.setEnabled(true);
        this.txtNombre.setEnabled(true);
        this.txtNumEmp.setEnabled(true);
        this.cmbNivel.setEnabled(true);
        this.txtPagoHora.setEnabled(true);
        this.btnCancelar.setEnabled(true);
        this.btnGuardar.setEnabled(true);
        this.btnLimpiar.setEnabled(true);
        this.btnMostrar.setEnabled(true);
        this.txtImpuesto.setEnabled(true);
        
    }
    
        public void deshabilitar(){
        this.txtClave.setEnabled(!true);
        this.txtNumEmp.setEnabled(!true);
        this.txtPuesto.setEnabled(!true);
        this.txtDom.setEnabled(!true);
        this.txtHoras.setEnabled(!true);
        this.cmbNivel.setEnabled(!true);
        this.txtPuesto.setEnabled(!true);
        this.txtImpuesto.setEnabled(!true);
        this.txtNombre.setEnabled(!true);
        this.txtPagoHora.setEnabled(!true);
        this.txtTotal.setEnabled(!true);
        this.txtPagoImp.setEnabled(!true);
        this.txtPagoAdi.setEnabled(!true);
        this.txtPagoTotal.setEnabled(!true);
        this.btnCancelar.setEnabled(!true);
        this.btnGuardar.setEnabled(!true);
        this.btnLimpiar.setEnabled(!true);
        this.btnMostrar.setEnabled(!true);
        
    }
         public void limpiar(){
            this.txtClave.setText("");
            this.txtDom.setText("");
            this.txtHoras.setText("");
            this.txtPuesto.setText("");
            this.txtPagoImp.setText("");
            this.txtNombre.setText("");
            this.txtNumEmp.setText("");
            this.txtPagoHora.setText("");
            this.txtPagoAdi.setText("");
            this.cmbNivel.setSelectedIndex(0);
            this.txtPuesto.setText("");
            this.txtTotal.setText("");
            this.txtPagoTotal.setText("");
            this.txtImpuesto.setText("");
        }
         public boolean validar(){
             boolean exito = false;
            if(this.txtClave.getText().equals("")) exito = true;
            if(this.txtDom.getText().equals("")) exito = true;
            if(this.txtHoras.getText().equals("")) exito = true;
            if(this.txtPuesto.getText().equals("")) exito = true;
            if(this.txtNombre.getText().equals("")) exito = true;
            if(this.txtPagoHora.getText().equals("")) exito = true;
            if(this.txtImpuesto.getText().equals("")) exito = true;
            if(this.txtNumEmp.getText().equals("")) exito = true;
            
            return exito;

         }



    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtNumEmp = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDom = new javax.swing.JTextField();
        txtPuesto = new javax.swing.JTextField();
        txtHoras = new javax.swing.JTextField();
        txtPagoHora = new javax.swing.JTextField();
        txtClave = new javax.swing.JTextField();
        cmbNivel = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtPagoTotal = new javax.swing.JTextField();
        txtPagoImp = new javax.swing.JTextField();
        txtPagoAdi = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        btnCerrar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        txtImpuesto = new javax.swing.JTextField();

        getContentPane().setLayout(null);

        jLabel1.setText("%Impuesto");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(70, 180, 65, 16);

        jLabel2.setText("Puesto");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(90, 160, 38, 16);

        jLabel3.setText("Domicilio");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(80, 120, 51, 16);

        jLabel4.setText("Nombre");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(80, 100, 45, 16);

        jLabel5.setText("Num. Empleado");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(30, 60, 100, 16);

        jLabel6.setText("Nivel");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(90, 240, 27, 16);

        jLabel7.setText("Pago por Hora");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(40, 270, 82, 16);

        jLabel8.setText("Horas Trabajadas");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(20, 290, 110, 16);

        txtNumEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumEmpActionPerformed(evt);
            }
        });
        getContentPane().add(txtNumEmp);
        txtNumEmp.setBounds(140, 60, 190, 22);

        txtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombre);
        txtNombre.setBounds(140, 100, 190, 22);

        txtDom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDomActionPerformed(evt);
            }
        });
        getContentPane().add(txtDom);
        txtDom.setBounds(140, 120, 190, 22);

        txtPuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtPuesto);
        txtPuesto.setBounds(140, 160, 190, 22);

        txtHoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtHorasActionPerformed(evt);
            }
        });
        getContentPane().add(txtHoras);
        txtHoras.setBounds(140, 290, 190, 22);

        txtPagoHora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoHoraActionPerformed(evt);
            }
        });
        getContentPane().add(txtPagoHora);
        txtPagoHora.setBounds(140, 270, 190, 22);

        txtClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClaveActionPerformed(evt);
            }
        });
        getContentPane().add(txtClave);
        txtClave.setBounds(140, 40, 190, 22);

        cmbNivel.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pre escolar", "Primaria", "Secundaria" }));
        getContentPane().add(cmbNivel);
        cmbNivel.setBounds(140, 240, 190, 22);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jLabel9.setText("Pago Impuestos (-)");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(10, 80, 120, 16);

        jLabel11.setText("Pago Adicional (+)");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(10, 50, 110, 16);

        jLabel12.setText("Pago Total");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(50, 110, 70, 16);

        jLabel13.setText("Total");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(90, 20, 29, 16);

        txtPagoTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoTotalActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoTotal);
        txtPagoTotal.setBounds(130, 110, 150, 22);

        txtPagoImp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoImpActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoImp);
        txtPagoImp.setBounds(130, 80, 150, 22);

        txtPagoAdi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPagoAdiActionPerformed(evt);
            }
        });
        jPanel1.add(txtPagoAdi);
        txtPagoAdi.setBounds(130, 50, 150, 22);

        txtTotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalActionPerformed(evt);
            }
        });
        jPanel1.add(txtTotal);
        txtTotal.setBounds(130, 20, 150, 22);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(30, 340, 300, 140);

        jLabel10.setText("Clave Contrato");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(40, 40, 84, 16);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(400, 420, 130, 30);

        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(390, 170, 140, 50);

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(390, 40, 140, 50);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(400, 380, 130, 30);

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(390, 100, 140, 50);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(400, 340, 130, 30);

        txtImpuesto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtImpuestoActionPerformed(evt);
            }
        });
        getContentPane().add(txtImpuesto);
        txtImpuesto.setBounds(140, 180, 190, 22);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNumEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumEmpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumEmpActionPerformed

    private void txtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreActionPerformed

    private void txtDomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDomActionPerformed

    private void txtPuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPuestoActionPerformed

    private void txtHorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtHorasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtHorasActionPerformed

    private void txtPagoHoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoHoraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoHoraActionPerformed

    private void txtClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClaveActionPerformed

    private void txtPagoTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoTotalActionPerformed

    private void txtPagoImpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoImpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoImpActionPerformed

    private void txtPagoAdiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPagoAdiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPagoAdiActionPerformed

    private void txtTotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        
       if (this.validar() == true) {
                
                JOptionPane.showMessageDialog(this, "Falto capturar información");
        } else {
            try {
                doc.setNumEmp(Integer.parseInt(this.txtNumEmp.getText()));
                doc.setDomicilio(this.txtDom.getText());
                doc.setHoras(Float.parseFloat(this.txtHoras.getText()));
                doc.setPagoHora(Float.parseFloat(this.txtPagoHora.getText()));
                doc.setNombre(this.txtNombre.getText());
                doc.setNivel(this.cmbNivel.getSelectedIndex());
                doc.setContrato(con);
                con.setClave(Integer.parseInt(this.txtClave.getText()));
                con.setPuesto(this.txtPuesto.getText());
                con.setImpuesto(Float.parseFloat(this.txtImpuesto.getText()));
               
                
         
            } catch (NumberFormatException e) {
                
                boolean exito = true;
                JOptionPane.showMessageDialog(this, "Surgio un error " + e.getMessage());
                this.deshabilitar();
                this.limpiar();
            }
            if (this.validar() == false) {
                this.btnMostrar.setEnabled(true);
                JOptionPane.showMessageDialog(this, "Se guardó la informacion con exito");
            }
        }

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        
        this.txtNumEmp.setText(String.valueOf(doc.getNumEmp()));
        this.txtPagoHora.setText(String.valueOf(doc.getPagoHora()));
        this.txtHoras.setText(String.valueOf(doc.getHoras()));
        this.txtDom.setText(doc.getDomicilio());
        this.txtNombre.setText(doc.getNombre());
        this.txtTotal.setText(String.valueOf(doc.getHoras()*doc.getPagoHora()));
        this.txtPagoImp.setText(String.valueOf(doc.calcularImpuesto()));
        this.txtPagoAdi.setText(String.valueOf(doc.calcularTotal()));
        this.txtPagoTotal.setText(String.valueOf((doc.getHoras()*doc.getPagoHora())+doc.calcularTotal()-doc.calcularImpuesto()));
        this.txtImpuesto.setText(String.valueOf(con.getImpuesto()));
        this.txtClave.setText(String.valueOf(con.getClave()));
        this.txtPuesto.setText(con.getPuesto());

    }//GEN-LAST:event_btnMostrarActionPerformed

    private void txtImpuestoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtImpuestoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtImpuestoActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        doc = new Docente();
        con = new Contrato();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.deshabilitar();
        this.limpiar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_btnCerrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbNivel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtClave;
    private javax.swing.JTextField txtDom;
    private javax.swing.JTextField txtHoras;
    private javax.swing.JTextField txtImpuesto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtNumEmp;
    private javax.swing.JTextField txtPagoAdi;
    private javax.swing.JTextField txtPagoHora;
    private javax.swing.JTextField txtPagoImp;
    private javax.swing.JTextField txtPagoTotal;
    private javax.swing.JTextField txtPuesto;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables

    private Docente doc;
    private Contrato con;

}
